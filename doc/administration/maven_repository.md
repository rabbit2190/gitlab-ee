# GitLab Maven Repository administration **[PREMIUM ONLY]**

>
[Introduced](https://gitlab.com/gitlab-org/gitlab-ee/issues/5811)
in GitLab 11.3. To learn how to use

When the GitLab Maven Repository is enabled, every project in GitLab will have
its own space to store [Maven](https://maven.apache.org/) packages.

To learn how to use it, see the [user documentation](../user/project/packages/maven_repository.md).

## Enabling the Maven Repository

NOTE: **Note:**
Once enabled, newly created projects will have the Packages feature enabled by
default. Existing projects will need to
[explicitly enabled it](../user/project/packages/maven_repository.md#enabling-the-packages-repository).

To enable the Maven repository you need to enable [Packages feature](packages.md)
